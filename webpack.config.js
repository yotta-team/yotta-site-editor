const path = require('path');

module.exports = {
    devtool: "source-map",
    entry: './src/editor.ts',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.ts?$/,
                loaders: [{ loader: 'babel-loader', options: { presets: ['env'] } }, 'ts-loader']
            }
        ]
    }
};