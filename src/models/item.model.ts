import { ItemMapModel } from './item-map.model';
import { ItemEditorModel } from './item-editor.model';

export class ItemModel {
    maps: Map<string, ItemMapModel[]>[] = [];
    items: ItemEditorModel[] = [];
}