export class ItemMapModel {
    key: string;
    index: number;
    subs?: ItemMapModel[];
}