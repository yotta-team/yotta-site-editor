import { ImageModel } from './image.model';
import { LinkModel } from './link.model';
import { TextModel } from './text.model';

export class ItemEditorModel {
    texts: TextModel[] = [];
    images: ImageModel[] = [];
    links: LinkModel[] = [];
    items: ItemEditorModel[] = [];
}