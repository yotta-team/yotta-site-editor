import { ItemEditorModel } from './models/item-editor.model';
import { ItemMapModel } from './models/item-map.model';
import { ImageModel } from './models/image.model';
import { LinkModel } from './models/link.model';
import { ItemModel } from './models/item.model';
import { TextModel } from './models/text.model';

export class ItemEditor {
    static canEditing(key: string, value: any) {
        return typeof (value) === 'object';
    }

    static getItem(object: any, root: string, modelItem?: ItemEditorModel): ItemModel {
        const item = new ItemEditorModel();
        const map = new Map<string, ItemMapModel[]>();
        if (object.src) {
            if (!object.src.startsWith(root))
                object.src = root + object.src;
            map.set('*', [{ key: 'images', index: modelItem ? modelItem.images.length : 0 }]);
            item.images.push(object);
        }
        else if (object.href) {
            map.set('*', [{ key: 'links', index: modelItem ? modelItem.links.length : 0 }]);
            item.links.push(object);
        }
        else if (typeof (object) === 'string') {
            map.set('*', [{ key: 'texts', index: modelItem ? modelItem.texts.length : 0 }]);
            item.texts.push({ text: object });
        }
        else if (Array.isArray(object)) {
            const model = new ItemModel();
            object.forEach(p => {
                const item = this.getItem(p, root, modelItem);
                model.items.push(item.items[0]);
                model.maps.push(item.maps[0]);
            });
            return model;
            //map[key] = { key: 'items', maps: results.map(r => r.map) };
            //item.items = results.map(r => r.item);
        }
        else {
            for (const key in object) {
                const prop = object[key];
                if (Array.isArray(prop)) {
                    const result = this.getItem(prop, root);
                    const itemMap = [];
                    result.maps.forEach((m, i) => {
                        itemMap.push({ key: 'items', index: item ? item.items.length + i : i, subs: m.get('*') } as ItemMapModel);
                    });
                    map.set(key, itemMap);
                    item.items = item.items.concat(result.items.splice(0));
                }
                else {
                    const result = this.getItem(prop, root, item);
                    item.images = item.images.concat(result.items[0].images);
                    item.texts = item.texts.concat(result.items[0].texts);
                    item.links = item.links.concat(result.items[0].links);
                    const itemMap = result.maps[0].get('*');
                    map.set(key, itemMap);
                    //map[key] = { key: 'items', maps: results.map(r => r.map) };
                }
            }
        }

        return {
            maps: [map],
            items: [item]
        } as ItemModel;
    }

    static getValue(map: ItemMapModel, item: ItemEditorModel, root: string) {
        switch (map.key) {
            case 'images':
                item[map.key][map.index].src = item[map.key][map.index].src.replace(root, '');
                return item[map.key][map.index];
            case 'links':
                return item[map.key][map.index];
            case 'texts':
                return item[map.key][map.index].text;
            case 'items':
                return map.subs.map(m => this.getValue(m, item[map.key][map.index], root));
        }
    }

    static getValueFrom(model: ItemModel, root: string) {
        const objects: any[] = [];
        model.items.forEach((item, i) => {
            objects.push({});
            model.maps[i].forEach((maps, key) => {
                maps.forEach(map => {
                    this.getValue(map, item, root);
                    if (key === '*') {
                        objects[i] = this.getValue(map, item, root);
                    }
                    else if (Array.isArray(objects[key])) {
                        objects[i][key] = objects[key].concat(this.getValue(map, item, root));
                    }
                    else {
                        objects[i][key] = this.getValue(map, item, root);
                    }
                });
            });
        });
        return objects.length == 1 ? objects[0] : objects;
    }

    constructor(private elem: Element, private key: string, private value: any, section: Element) {
        this.startEditor(elem, section);
    }

    startEditor(elem: Element, section: Element) {
        const root = section.getAttribute('data-root');
        const dialog: any = document.querySelector('#editor-dialog');
        const editor: any = dialog.querySelector('yo-editor');

        const titleElem = section.querySelector(`#${section.id}_title`);
        const title = titleElem ? titleElem.textContent : '';

        elem.classList.add('yo-editable');
        elem.addEventListener('click', (e) => {
            e.preventDefault();
            e.stopPropagation();
            const result = ItemEditor.getItem(this.value, root);
            dialog.querySelector('.title').textContent = title || '';
            editor.items = result.items;
            editor.maps = result.maps;
            dialog.data = { hash: section.id, key: this.key };
        });
    }
}