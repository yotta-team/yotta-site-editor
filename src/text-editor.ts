export class TextEditor {
    static canEditing(key: string, value: any) {
        return typeof (value) === 'string';
    }

    constructor(private elem: Element, private key: string, private value: any, section: Element) {
        this.startEditor(elem, section);
    }

    startEditor(elem, section) {
        elem.classList.add('yo-editable');
        elem.setAttribute('contenteditable', true);

        elem.addEventListener('focus', (e) => {
            const editor: any = document.querySelector('yo-text');
            if (!elem.getAttribute('cache'))
                elem.setAttribute('cache', elem.innerHTML);
            editor.targetElement = elem;
        });

        elem.addEventListener('blur', (e) => {
            if (elem.innerHTML === elem.getAttribute('cache'))
                return;

            const value = elem.innerHTML;
            const hash = section.id;
            elem.removeAttribute('cache');
            window.postMessage({ event: 'contentChanged', args: { hash, key: this.key, value } }, window.location.origin);
        });
    }
}