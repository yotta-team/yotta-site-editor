import { expect, use, assert } from 'chai';
import { stub, SinonStub } from 'sinon';
import { JSDOM, DOMWindow } from 'jsdom';

import { } from 'mocha';

import { ItemEditor } from './item-editor';
import { ItemMapModel } from './models/item-map.model';

declare var window: DOMWindow;
declare var document: Document;

describe('Editor module', function () {
    let window: DOMWindow;
    let document: Document;

    before(function (done) {
        JSDOM.fromFile(__dirname + '/editor.spec.html')
            .then((jsdom) => {
                window = jsdom.window;
                document = window.document;
                done();
            });
    });

    it('dom ready', function () {
        expect(window).to.be.not.equals(undefined);
        expect(document).to.be.not.equals(undefined);
        expect(document.body).to.be.not.equals(undefined);
    });

    /**
     * Test is can or can't edit
     */

    it('ItemEditor can edit link object', function () {
        const key = 'test';
        const value = { href: 'http://test.com', text: 'test' };
        const result = ItemEditor.canEditing(key, value);

        expect(result).to.be.equals(true);
    });

    it('ItemEditor can edit image object', function () {
        const key = 'test';
        const value = { src: 'http://test.com/image.png', alt: 'test' };
        const result = ItemEditor.canEditing(key, value);

        expect(result).to.be.equals(true);
    });

    it('ItemEditor can edit custom object', function () {
        const key = 'test';
        const value = {
            text: 'text editable',
            link: { href: 'http://test.com', text: 'test' },
            image: { src: 'http://test.com/image.png', alt: 'test' }
        };
        const result = ItemEditor.canEditing(key, value);

        expect(result).to.be.equals(true);
    });

    it('ItemEditor can edit array', function () {
        const key = 'test';
        const value = [];
        const result = ItemEditor.canEditing(key, value);

        expect(result).to.be.equals(true);
    });

    it('ItemEditor can\'t edit string', function () {
        const key = 'test';
        const value = '';
        const result = ItemEditor.canEditing(key, value);

        expect(result).to.be.equals(false);
    });

    /**
     * Test with single values
     */

    it('ItemEditor get item with only a link object', function () {
        const key = 'test';
        const root = 'root';
        const value = { href: 'http://test.com', text: 'test' };
        const result = ItemEditor.getItem(value, root);

        //console.log(result);
        expect(result.items.length).to.be.equals(1);
        expect(result.maps.length).to.be.equals(1);
        const item = result.items[0];
        expect(item.links.length).to.be.equals(1);
        expect(item.links[0].href).to.be.equals(value.href);
        expect(item.links[0].text).to.be.equals(value.text);

        const maps = result.maps[0].get('*');
        expect(maps.length).to.be.equals(1);
        expect(maps[0].index).to.be.equals(0);
        expect(maps[0].key).to.be.equals('links');

        const rollback = ItemEditor.getValueFrom(result, root);
        //console.log(rollback);
        expect(JSON.stringify(rollback)).to.be.equals(JSON.stringify(value));
    });

    it('ItemEditor get item with only a image object', function () {
        const key = 'test';
        const root = 'root';
        const value = { src: 'http://test.com/image.png', alt: 'test' };
        const result = ItemEditor.getItem(value, root);

        //console.log(result);
        expect(result.items.length).to.be.equals(1);
        expect(result.maps.length).to.be.equals(1);
        const item = result.items[0];
        expect(item.images.length).to.be.equals(1);
        expect(item.images[0].src).to.be.equals(value.src);
        expect(item.images[0].alt).to.be.equals(value.alt);

        const maps = result.maps[0].get('*');
        expect(maps.length).to.be.equals(1);
        expect(maps[0].index).to.be.equals(0);
        expect(maps[0].key).to.be.equals('images');

        const rollback = ItemEditor.getValueFrom(result, root);
        //console.log(rollback);
        expect(JSON.stringify(rollback)).to.be.equals(JSON.stringify(value));
    });

    it('ItemEditor get item with only a complex object', function () {
        const key = 'test';
        const root = 'root';
        const value = {
            text: 'text editable',
            link: { href: 'http://test.com', text: 'test' },
            image: { src: 'http://test.com/image.png', alt: 'test' }
        };
        const result = ItemEditor.getItem(value, root);

        //console.log(JSON.stringify(result));
        expect(result.items.length).to.be.equals(1);
        expect(result.maps.length).to.be.equals(1);

        const item = result.items[0];
        expect(item.images.length).to.be.equals(1);
        expect(item.links.length).to.be.equals(1);
        expect(item.texts.length).to.be.equals(1);
        expect(item.texts[0].text).to.be.equals(value.text);
        expect(item.links[0].href).to.be.equals(value.link.href);
        expect(item.links[0].text).to.be.equals(value.link.text);
        expect(item.images[0].src).to.be.equals(value.image.src);
        expect(item.images[0].alt).to.be.equals(value.image.alt);

        result.maps[0].forEach((maps: ItemMapModel[], key: string) => {
            //console.log(key, value);
            expect(maps.length).to.be.equals(1);
            expect(item[maps[0].key]).to.be.not.equals(undefined);
            expect(item[maps[0].key].length).to.be.greaterThan(maps[0].index);
        });

        const rollback = ItemEditor.getValueFrom(result, root);
        //console.log(rollback);
        expect(JSON.stringify(rollback)).to.be.equals(JSON.stringify(value));
    });

    it('ItemEditor get item with only a complex object with multiple values', function () {
        const key = 'test';
        const root = 'root';
        const textsLength = 3;
        const imagesLength = 5;
        const linksLength = 4;
        const value = {
            text1: 'text1 editable',
            text2: 'text2 editable',
            text3: 'text3 editable',
            link1: { href: 'http://test1.com', text: 'test1' },
            link2: { href: 'http://test2.com', text: 'test2' },
            link3: { href: 'http://test3.com', text: 'test3' },
            link4: { href: 'http://test4.com', text: 'test4' },
            image1: { src: 'http://test.com/image1.png', alt: 'test1' },
            image2: { src: 'http://test.com/image2.png', alt: 'test2' },
            image3: { src: 'http://test.com/image3.png', alt: 'test3' },
            image4: { src: 'http://test.com/image4.png', alt: 'test4' },
            image5: { src: 'http://test.com/image5.png', alt: 'test5' }
        };
        const result = ItemEditor.getItem(value, root);

        //console.log(result.items[0]);
        expect(result.items.length).to.be.equals(1);
        expect(result.maps.length).to.be.equals(1);

        const item = result.items[0];
        expect(item.images.length).to.be.equals(imagesLength);
        expect(item.links.length).to.be.equals(linksLength);
        expect(item.texts.length).to.be.equals(textsLength);
        for (let i = 0; i < textsLength; i++) {
            expect(item.texts[i].text).to.be.equals(value[`text${i + 1}`]);
        }
        for (let i = 0; i < linksLength; i++) {
            expect(item.links[i].href).to.be.equals(value[`link${i + 1}`].href);
            expect(item.links[i].text).to.be.equals(value[`link${i + 1}`].text);
        }
        for (let i = 0; i < imagesLength; i++) {
            expect(item.images[i].src).to.be.equals(value[`image${i + 1}`].src);
            expect(item.images[i].alt).to.be.equals(value[`image${i + 1}`].alt);
        }

        result.maps[0].forEach((maps: ItemMapModel[], key: string) => {
            //console.log(key, value);
            expect(maps.length).to.be.equals(1);
            expect(item[maps[0].key]).to.be.not.equals(undefined);
            expect(item[maps[0].key].length).to.be.greaterThan(maps[0].index);
        });

        const rollback = ItemEditor.getValueFrom(result, root);
        //console.log(rollback);
        expect(JSON.stringify(rollback)).to.be.equals(JSON.stringify(value));
    });

    /**
     * Test with sub list of values
     */

    it('ItemEditor get item with only a complex object with a list', function () {
        const key = 'test';
        const root = 'root';
        const value = {
            text: 'text editable',
            texts: [
                'text editable',
                'text editable',
                'text editable'
            ],
            link: { href: 'http://test1.com', text: 'test 1' },
            links: [
                { href: 'http://test1.com', text: 'test 1' },
                { href: 'http://test2.com', text: 'test 2' },
                { href: 'http://test3.com', text: 'test 3' }
            ],
            image: { src: 'http://test.com/image.png', alt: 'test' },
            images: [
                { src: 'http://test.com/image1.png', alt: 'test1' },
                { src: 'http://test.com/image2.png', alt: 'test2' },
                { src: 'http://test.com/image3.png', alt: 'test3' },
            ]
        };
        const result = ItemEditor.getItem(value, root);

        //result.items[0].items.forEach(item => console.log(item));
        expect(result.items.length).to.be.equals(1);
        expect(result.maps.length).to.be.equals(1);

        const item = result.items[0];
        expect(item.images.length).to.be.equals(1);
        expect(item.links.length).to.be.equals(1);
        expect(item.texts.length).to.be.equals(1);
        expect(item.items.length).to.be.equals(value.links.length + value.images.length + value.texts.length);
        expect(item.texts[0].text).to.be.equals(value.text);
        expect(item.images[0].src).to.be.equals(value.image.src);
        expect(item.images[0].alt).to.be.equals(value.image.alt);
        expect(item.links[0].href).to.be.equals(value.link.href);
        expect(item.links[0].text).to.be.equals(value.link.text);
        let countLinks = 0, countTexts = 0, countImages = 0;
        for (let i = 0; i < item.items.length; i++) {
            if (item.items[i].links.length > 0) {
                expect(item.items[i].links[0].href).to.be.equals(value.links[countLinks].href);
                expect(item.items[i].links[0].text).to.be.equals(value.links[countLinks].text);
                countLinks++;
            }
            if (item.items[i].texts.length > 0) {
                expect(item.items[i].texts[0].text).to.be.equals(value.texts[countTexts]);
                countTexts++;
            }
            if (item.items[i].images.length > 0) {
                expect(item.items[i].images[0].src).to.be.equals(value.images[countImages].src);
                expect(item.items[i].images[0].alt).to.be.equals(value.images[countImages].alt);
                countImages++;
            }
        }

        expect(countTexts).to.be.equals(value.texts.length);
        expect(countLinks).to.be.equals(value.links.length);
        expect(countImages).to.be.equals(value.images.length);

        result.maps[0].forEach((maps: ItemMapModel[], key: string) => {
            //console.log(key, maps);
            expect(item[maps[0].key]).to.be.not.equals(undefined);
            if (maps[0].key === 'items')
                expect(maps.length).to.be.equals(3);
            else
                expect(maps.length).to.be.equals(1);
            expect(item[maps[0].key].length).to.be.greaterThan(maps[0].index);
        });

        const rollback = ItemEditor.getValueFrom(result, root);
        //console.log(rollback);
        expect(JSON.stringify(rollback)).to.be.equals(JSON.stringify(value));
    });

    it('ItemEditor get item with only a complex object with a list and sublists', function () {
        const key = 'test';
        const root = 'root';
        const value = {
            text: 'text editable',
            objs: [{
                link: { href: 'http://test1.com', text: 'test 1' },
                texts: [
                    'text editable',
                    'text editable',
                    'text editable'
                ]
            } as any,
            {
                image: { src: 'http://test.com/image.png', alt: 'test' },
                links: [
                    { href: 'http://test1.com', text: 'test 1' },
                    { href: 'http://test2.com', text: 'test 2' },
                    { href: 'http://test3.com', text: 'test 3' },
                    { href: 'http://test2.com', text: 'test 2' },
                ],
            } as any,
            {
                text: 'text editable',
                images: [
                    { src: 'http://test.com/image1.png', alt: 'test1' },
                    { src: 'http://test.com/image2.png', alt: 'test2' },
                ]
            } as any]
        };
        const result = ItemEditor.getItem(value, root);

        //value.objs.forEach(item => console.log(item));
        expect(result.items.length).to.be.equals(1);
        expect(result.maps.length).to.be.equals(1);

        const item = result.items[0];
        expect(item.items[0].links.length).to.be.equals(1);
        expect(item.items[0].items.length).to.be.equals(value.objs[0].texts.length);
        expect(item.items[0].items.every(a => a.texts.length > 0)).to.be.equals(true);

        expect(item.items[1].images.length).to.be.equals(1);
        expect(item.items[1].items.length).to.be.equals(value.objs[1].links.length);
        expect(item.items[1].items.every(a => a.links.length > 0)).to.be.equals(true);

        expect(item.items[2].texts.length).to.be.equals(1);
        expect(item.items[2].items.length).to.be.equals(value.objs[2].images.length);
        expect(item.items[2].items.every(a => a.images.length > 0)).to.be.equals(true);

        result.maps[0].forEach((maps: ItemMapModel[], key: string) => {
            //console.log(key, maps);
            expect(item[maps[0].key]).to.be.not.equals(undefined);
            if (maps[0].key === 'items')
                expect(maps.length).to.be.equals(3);
            else
                expect(maps.length).to.be.equals(1);
            expect(item[maps[0].key].length).to.be.greaterThan(maps[0].index);
        });
    });

    /**
     * Test with multiple values
     */

    it('ItemEditor get item with a list of links', function () {
        const key = 'test';
        const root = 'root';
        const value = [
            { href: 'http://test1.com', text: 'test 1' },
            { href: 'http://test2.com', text: 'test 2' },
            { href: 'http://test3.com', text: 'test 3' }
        ];
        const result = ItemEditor.getItem(value, root);

        //console.log(result);
        expect(result.items.length).to.be.equals(value.length);
        expect(result.maps.length).to.be.equals(value.length);
        for (let i = 0; i < value.length; i++) {
            const item = result.items[i];
            expect(item.links.length).to.be.equals(1);
            expect(item.links[0].href).to.be.equals(value[i].href);
            expect(item.links[0].text).to.be.equals(value[i].text);
            const maps = result.maps[i].get('*');
            expect(maps.length).to.be.equals(1);
            expect(maps[0].index).to.be.equals(0);
            expect(maps[0].key).to.be.equals('links');
        }
    });

    it('ItemEditor get item with a list of image objects', function () {
        const key = 'test';
        const root = 'root';
        const value = [
            { src: 'http://test.com/image1.png', alt: 'test1' },
            { src: 'http://test.com/image2.png', alt: 'test2' },
            { src: 'http://test.com/image3.png', alt: 'test3' },
        ];
        const result = ItemEditor.getItem(value, root);

        //console.log(result);
        expect(result.items.length).to.be.equals(value.length);
        expect(result.maps.length).to.be.equals(value.length);
        for (let i = 0; i < value.length; i++) {
            const item = result.items[i];
            expect(item.images.length).to.be.equals(1);
            expect(item.images[0].src).to.be.equals(value[i].src);
            expect(item.images[0].alt).to.be.equals(value[i].alt);
            const maps = result.maps[i].get('*');
            expect(maps.length).to.be.equals(1);
            expect(maps[0].index).to.be.equals(0);
            expect(maps[0].key).to.be.equals('images');
        }
    });

    it('ItemEditor get item with a list of complex objects', function () {
        const key = 'test';
        const root = 'root';
        const value = [{
            text: 'text1 editable',
            link: { href: 'http://test1.com', text: 'test' },
            image: { src: 'http://test.com/image1.png', alt: 'test' }
        },
        {
            text: 'text2 editable',
            link: { href: 'http://test2.com', text: 'test' },
            image: { src: 'http://test.com/image2.png', alt: 'test' }
        },
        {
            text: 'text3 editable',
            link: { href: 'http://test3.com', text: 'test' },
            image: { src: 'http://test.com/image3.png', alt: 'test' }
        }];
        const result = ItemEditor.getItem(value, root);

        //console.log(JSON.stringify(result));
        expect(result.items.length).to.be.equals(value.length);
        expect(result.maps.length).to.be.equals(value.length);

        for (let i = 0; i < value.length; i++) {
            const item = result.items[i];
            expect(item.images.length).to.be.equals(1);
            expect(item.links.length).to.be.equals(1);
            expect(item.texts.length).to.be.equals(1);
            expect(item.texts[0].text).to.be.equals(value[i].text);
            expect(item.links[0].href).to.be.equals(value[i].link.href);
            expect(item.links[0].text).to.be.equals(value[i].link.text);
            expect(item.images[0].src).to.be.equals(value[i].image.src);
            expect(item.images[0].alt).to.be.equals(value[i].image.alt);

            result.maps[i].forEach((maps: ItemMapModel[], key: string) => {
                //console.log(key, value);
                expect(maps.length).to.be.equals(1);
                expect(item[maps[0].key]).to.be.not.equals(undefined);
                expect(item[maps[0].key].length).to.be.greaterThan(maps[0].index);
            });
        }
    });
});