import { TextEditor } from './text-editor';
import { ItemEditor } from './item-editor';

function mapEditor(elem: Element, key: string, value: any, section: Element) {
    if (!elem) return;

    const editors = [TextEditor, ItemEditor];
    for (let i = 0; i < editors.length; i++) {
        if (editors[i].canEditing(key, value)) {
            const editor = new editors[i](elem, key, value, section);
            (<any>elem).editor = editor;
            return;
        }
    }
}

function mapContent(content, section) {
    for (const key in content.data) {
        mapEditor(document.querySelector(`#${content.hash}_${key}`), key, content.data[key], section);
    }
}

function init(metadata, root) {
    metadata.content.forEach(content => {
        const section = document.querySelector(`#${content.hash}`);
        const title = section.querySelector(`#${content.hash}_title`);
        if (title) {
            const tooltip = document.createElement('div');
            const tooltipText = document.createElement('div');
            tooltipText.textContent = title.textContent;
            tooltipText.classList.add('yo-tooltip-text');
            tooltip.classList.add('yo-section-tooltip');
            tooltip.appendChild(tooltipText);
            section.insertBefore(tooltip, section.childNodes[0]);
        }
        section.classList.add('yo-section');
        section.setAttribute('data-root', root);
        mapContent(content, section);
    });

    const dialog: any = document.querySelector('#editor-dialog');
    const editor: any = dialog.querySelector('yo-editor');
    editor.addEventListener('ready', e => {
        dialog.open();
    });
    dialog.addEventListener('opened-changed', (e) => {
        if (e.detail.value) {
            window.postMessage({ event: 'hideToolbar' }, window.location.origin);
        }
    });
    dialog.addEventListener('iron-overlay-closed', (e) => {
        window.postMessage({ event: 'showToolbar' }, window.location.origin);
        editor.items = [];
    });
    dialog.querySelector(`[dialog-confirm]`).addEventListener('click', (e) => {
        const args: any = {};
        const maps = editor.maps;
        const items = editor.getItems();

        args.hash = dialog.data.hash;
        args.key = dialog.data.key;
        args.value = ItemEditor.getValueFrom({ maps, items }, root);
        const section = document.querySelector(`#${args.hash}`);
        section.classList.add('loading');

        window.postMessage({ event: 'contentChanged', args }, window.location.origin);
    });
}

function updateHTML(elem, content, root) {
    for (const key in content) {
        const child = elem.querySelector(`#${elem.id}-${key}`);
        if (typeof (content[key]) === 'string') {
            child.innerHTML = content[key];
        }
        else if (content[key].href) {
            child.href = content[key].href;
            child.textContent = content[key].textContent;
        }
        else if (content[key].src) {
            child.src = root + content[key].src;
        }
        else if (content[key].length) {
            updateHTML(child, content[key], root);
        }
        else {
            updateHTML(child, content[key], root);
        }
    }
}

function receiveMessage(event) {
    switch (event.data.event) {
        case 'metadata':
            init(event.data.args.metadata, event.data.args.root || '');
            break;
        case 'contentUpdated':
            if (event.data.args.success) {
                showToast('Page updated with success!');
                if (event.data.args.value) {
                    const section = document.querySelector(`#${event.data.args.hash}`);
                    const elem = section.querySelector(`#${event.data.args.hash}_${event.data.args.key}`);
                    (<any>elem).editor.value = event.data.args.value;
                    updateHTML(elem, event.data.args.value, section.getAttribute('data-root'));
                }
            }
            else {
                showToast('An error ocurred.');
            }
            break;
        default:
            console.log('event unknown ', event);
    }
}

function showToast(message) {
    const toast: any = document.querySelector('#toast');
    toast.text = message;
    toast.open();
}


console.log('TESTE');
window.addEventListener('message', receiveMessage, false);
window.postMessage({ event: 'ready', args: {} }, window.location.origin);
